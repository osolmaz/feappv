! Id:$
subroutine uplot(ct)

  !      * * F E A P * * A Finite Element Analysis Program

  !....  Copyright (c) 1984-2012: Robert L. Taylor
  !                               All rights reserved

  !-----[--.----+----.----+----.-----------------------------------------]
  !      Purpose:

  !      Inputs:

  !      Outputs:
  !-----[--.----+----.----+----.-----------------------------------------]

  implicit  none

  real*8 ::    ct(3)

  write(*,*) ' *WARNING* No user plot available.'

end subroutine uplot
