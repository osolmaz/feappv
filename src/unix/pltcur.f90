! Id: pltcur.f,v 1.1 2000/08/24 20:49:58 rlt Exp $
subroutine pltcur()

  !      * * F E A P * * A Finite Element Analysis Program

  !....  Copyright (c) 1984-2012: Robert L. Taylor
  !                               All rights reserved

  !-----[--+---------+---------+---------+---------+---------+---------+-]

  !     Routine to turn on screen cursor for text window
  !     N.B. Not needed for UNIX version

end subroutine pltcur
